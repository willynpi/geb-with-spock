## Introduction ##

Geb is a selenium based tool for automatic UI testing, using Groovy which is a Java lang that is easier to learn. And Spock is a unit test framework. 

## Set up ##

For a Geb project, you need to install [Java SE Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/index.html), [groovy](http://groovy-lang.org/download.html).
 
## Run the project ##

Open the terminal, go under the project, run it by command lines.


```
#!command line

$groovy test_name.groovy

```